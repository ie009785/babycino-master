class TestBugE2{

    //Bug ==( > has lower predence than &&)
    public static void main(String[] arg){

        System.out.println(new Test().T());
    }
}

class Test {

    public int T(){
        int x;
        int y;
        int z;
        x = 5;
        y = 0;
        z = 10;

        while( x > y && z > x){
            y = y +1;
            z = z -1;
        }
        System.out.println(z);
        return y;
    }
}
// > doesn't has lower predence than &&
// if so, it should output error message said that y and z expect to be boolean
